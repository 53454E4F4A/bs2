
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>
		
#define PRODUCERTIMEOUT 3	
#define CONSUMERTIMEOUT 2	
#define CAPS 1

typedef struct  {
  int caps;
  pthread_mutex_t lock;  
} thread_args;


pthread_mutex_t consumer_lock = PTHREAD_MUTEX_INITIALIZER;
thread_args consumer1_args;
thread_args consumer2_args;
pthread_t producer1;						
pthread_t producer2;										
pthread_t consumer;									
pthread_t hcontroller;

void *producer(void *args);
void *consuming(void *lock);
void *controller(void *args);