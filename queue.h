#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>

#define BUFFERSIZE 10

typedef struct
{
  char buffer[BUFFERSIZE];
  int head;
  int tail;
  sem_t free;
  sem_t occupied;
  pthread_mutex_t mtx_buffer;
  
} my_queue;

my_queue *queue;

void my_queue_init(void);
void my_queue_add(char character);
char my_queue_next(void);
void my_queue_cleanup(void);