
#include "threads.h"
#include "queue.h"
 
int main() {
  
     
 
  
  consumer1_args.caps = 0;
  consumer2_args.caps = CAPS;
  pthread_mutex_init(&consumer1_args.lock, NULL);	
  pthread_mutex_init(&consumer2_args.lock, NULL);
  my_queue_init();
  pthread_create(&producer1, NULL, producer, &consumer1_args);	
  pthread_create(&producer2, NULL, producer, &consumer2_args);
  
  pthread_create(&consumer, NULL, consuming, NULL);	
  pthread_create(&hcontroller, NULL, controller, NULL);
  pthread_join(producer1, NULL);				
  pthread_join(producer2, NULL);							
  pthread_join(consumer, NULL);							
  pthread_join(hcontroller, NULL);
  return 0;
}

void cleanup() {

 		
 pthread_mutex_destroy(&consumer1_args.lock);		
 pthread_mutex_destroy(&consumer2_args.lock);		
 pthread_mutex_destroy(&consumer_lock);		
  
}

void *producer(void *args)
{
    
	char product = '\0';
	if(((thread_args*)args)->caps == CAPS) product = 'A';
	else product = 'a';									
	
	while(1)										
	{
	  
		pthread_mutex_lock(&(((thread_args*)args)->lock));			
		my_queue_add(product);							
		product = (product == 'z' || product == 'Z') ? product-25 : product+1;	
		pthread_mutex_unlock(&(((thread_args*)args)->lock));			
		sleep(PRODUCERTIMEOUT);							
	}
	return NULL;
}

void *consuming(void *args)
{	
	while(1)								
	{
		pthread_mutex_lock(&consumer_lock);		
		my_queue_next();						
		pthread_mutex_unlock(&consumer_lock);
		sleep(CONSUMERTIMEOUT);					
	}
	return NULL;
}

void *controller(void *args)
{
	char input;						
	int producer1BlockedFlag = 0;	
	int producer2BlockedFlag = 0;	
	int consumerBlockedFlag = 0;	
	
	while(1)
	{
		scanf("%c", &input);
		switch(input)
		{
			
			case '1':
				if(!producer1BlockedFlag)					
				{
					pthread_mutex_lock(&consumer1_args.lock);	
					//printf("%d\n",pthread_mutex_lock(&consumer1_args.lock));
					producer1BlockedFlag = 1;
					printf("producer1 stopped\n");
				}
				else										
				{
					pthread_mutex_unlock(&consumer1_args.lock);	
					producer1BlockedFlag = 0;
					printf("producer1 started\n");
				}
				break;

			
			case '2':
				if(!producer2BlockedFlag)					
				{
					pthread_mutex_lock(&consumer2_args.lock);	
					producer2BlockedFlag = 1;
					printf("producer2 stopped\n");
				}
				else										
				{
					pthread_mutex_unlock(&consumer2_args.lock);	
					producer2BlockedFlag = 0;
					printf("producer2 started\n");
				}
				break;

			
			case 'c':
			case 'C':
				if(!consumerBlockedFlag)					
				{
					pthread_mutex_lock(&consumer_lock);		
					consumerBlockedFlag = 1;
					printf("consumer stopped\n");
				}
				else										
				{
					pthread_mutex_unlock(&consumer_lock);	
					consumerBlockedFlag = 0;
					printf("consumer started\n");
				}
				break;

			
			case 'q':
			case 'Q':
			   
			    pthread_cancel(producer1);
			    pthread_cancel(producer2);
			    pthread_cancel(consumer);
				
				my_queue_cleanup();						
				cleanup();		
		            
				pthread_exit(NULL);							
				break;
		

		}
	}
	return NULL;
}
