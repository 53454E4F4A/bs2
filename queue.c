#include "queue.h"

void my_queue_init(void) {
  
  queue = (my_queue*) malloc(sizeof(my_queue));
  pthread_mutex_init(&queue->mtx_buffer, NULL);
  queue->head = 0;
  queue->tail = 0;
  sem_init(&queue->free, 0 , BUFFERSIZE);
  sem_init(&queue->occupied, 0 , 0);
  
}
void my_queue_add(char character) {
      int i = -1, j = -1;
      if(!sem_trywait(&queue->free))
      {
	
	pthread_mutex_lock(&queue->mtx_buffer);
	sem_getvalue(&queue->free, &i);
	sem_getvalue(&queue->occupied, &j);
	printf("sem value: %d head: %d tail: %d: sem value: occ: %d \n", i, queue->head, queue->tail, j);
	queue->buffer[queue->head] = character;			
	queue->head = (queue->head + 1) % BUFFERSIZE;	
	pthread_mutex_unlock(&queue->mtx_buffer);
	sem_post(&queue->occupied);
	printf("wrote %c\n", character);		
      }
   
}
char my_queue_next(void) {
  char character = '\0'; 
	 int i = -1, j = -1;
	if(!sem_trywait(&queue->occupied))
	{
	
		pthread_mutex_lock(&queue->mtx_buffer);	
		sem_getvalue(&queue->free, &i);
		sem_getvalue(&queue->occupied, &j);
	//printf("sem value: %d head: %d tail: %d: sem value: occ: %d \n", i, queue->head, queue->tail, j);
		character = queue->buffer[queue->tail];
		queue->tail = ((queue->tail) + 1) % BUFFERSIZE;	
		pthread_mutex_unlock(&queue->mtx_buffer);				
		sem_post(&queue->free);								
		printf("read %c\n", character);						
	}
	return character;
}	
void my_queue_cleanup(void) {
  pthread_mutex_destroy(&queue->mtx_buffer);
  sem_destroy(&queue->free);		
  sem_destroy(&queue->occupied);	
  
  
}
